# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Adam Banasiak <elserwis@gmail.com>, 2014
# Adam Łukasiak <adamus160@gmail.com>, 2014
# EDP <maciej@eduprojekt.org>, 2014
# Jakub Kopczyk <kuba5566@gmail.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: edx-platform\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-11-24 16:23-0500\n"
"PO-Revision-Date: 2014-12-10 12:21+0000\n"
"Last-Translator: EDP <maciej@eduprojekt.org>\n"
"Language-Team: Polish (http://www.transifex.com/projects/p/edx-platform/language/pl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pl\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. Translators: Simply move the percent symbol (%) to the correct location. Do
#. NOT translate the word statistic.
#: analytics_dashboard/templatetags/dashboard_extras.py
msgid "{statistic}%"
msgstr "{statistic}%"

#: courses/presenters.py
msgid "Female"
msgstr "Kobieta"

#: courses/presenters.py
msgid "Male"
msgstr "Mężczyzna"

#. Translators: Other gender
#. Translators: This describes the learner's education level.
#: courses/presenters.py courses/presenters.py
msgid "Other"
msgstr "Inne"

#. Translators: Unknown gender
#. Translators: This describes the learner's education level.
#: courses/presenters.py courses/presenters.py courses/presenters.py
msgid "Unknown"
msgstr "Nieznane"

#. Translators: This describes the learner's education level.
#: courses/presenters.py
msgid "None"
msgstr "Brak"

#. Translators: This describes the learner's education level (e.g. Elementary
#. School Degree).
#: courses/presenters.py
msgid "Primary"
msgstr "Podstawowe"

#. Translators: This describes the learner's education level  (e.g. Middle
#. School Degree).
#: courses/presenters.py
msgid "Middle"
msgstr "Gimnazjum"

#. Translators: This describes the learner's education level.
#: courses/presenters.py
msgid "Secondary"
msgstr "Średnie"

#. Translators: This describes the learner's education level (e.g. Associate's
#. Degree).
#: courses/presenters.py
msgid "Associate"
msgstr "Policealne"

#. Translators: This describes the learner's education level (e.g. Bachelor's
#. Degree).
#: courses/presenters.py
msgid "Bachelor's"
msgstr "Licencjat"

#. Translators: This describes the learner's education level (e.g. Master's
#. Degree).
#: courses/presenters.py
msgid "Master's"
msgstr "Magisterium"

#. Translators: This describes the learner's education level (e.g. Doctorate
#. Degree).
#: courses/presenters.py
msgid "Doctorate"
msgstr "Doktorat"

#. Translators: This is a placeholder for enrollment data collected without a
#. known geolocation.
#: courses/presenters.py
msgid "Unknown Country"
msgstr "Kraj nieznany"

#. Translators: This describes the data displayed in the chart below.
#: courses/templates/courses/demographics_chart_info.html
msgid "Students (Self-Reported)"
msgstr "Studenci (wg zgłoszeń)"

#: courses/templates/courses/engagement_content.html
msgid "Weekly Student Engagement"
msgstr "Tygodniowe zaangażowanie studentów"

#: courses/templates/courses/engagement_content.html
msgid "How many students are interacting with my course?"
msgstr "Jak wielu studentów bierze udział w kursie?"

#: courses/templates/courses/engagement_content.html
msgid "Students"
msgstr "Studentów"

#: courses/templates/courses/engagement_content.html
msgid ""
"The number of active students, and the number of students who engaged in "
"specific activities, over time."
msgstr ""
"Liczba aktywnych studentów oraz liczba studentów zaangażowanych w określone "
"czynności, w przedziale czasu."

#: courses/templates/courses/engagement_content.html
msgid "Student Activity Metrics"
msgstr "Pomiar aktywności studentów"

#: courses/templates/courses/engagement_content.html
msgid "Active Students Last Week"
msgstr "Aktywnych studentów w zeszłym tygodniu"

#: courses/templates/courses/engagement_content.html
msgid "Students who visited at least one page in the course content."
msgstr "Studenci, którzy odwiedzili przynajmniej jedną stronę z kursu."

#. Translators: This is a label indicating the number of students who watched
#. a video.
#: courses/templates/courses/engagement_content.html
msgid "Watched a Video Last Week"
msgstr "Obejrzeli wideo w poprzednim tygodniu"

#: courses/templates/courses/engagement_content.html
msgid "Students who played one or more videos."
msgstr "Studenci, którzy obejrzeli jeden lub więcej filmów."

#. Translators: This is a label indicating the number of students who tried a
#. problem.
#: courses/templates/courses/engagement_content.html
msgid "Tried a Problem Last Week"
msgstr "Podeszli do ćwiczenia"

#: courses/templates/courses/engagement_content.html
msgid ""
"Students who submitted an answer for a standard problem. Not all problem "
"types are included."
msgstr ""
"Studenci, którzy wprowadzili odpowiedź na podstawowe pytanie. Lista nie "
"zawiera wszystkich ćwiczeń."

#. Translators: This is a label indicating the number of students who posted
#. in a forum discussion.
#: courses/templates/courses/engagement_content.html
msgid "Posted in a Discussion Last Week"
msgstr "Założyli nowy temat na forum"

#: courses/templates/courses/engagement_content.html
msgid "Students who contributed to any discussion topic."
msgstr "Studenci, którzy zamieścili wpis w dowolnym temacie."

#: courses/templates/courses/engagement_content.html
#: courses/templates/courses/engagement_content.html
msgid "Content Engagement Breakdown"
msgstr "Struktura zaangażowania w treść"

#: courses/templates/courses/engagement_content.html
#: courses/templates/courses/enrollment_activity.html
#: courses/templates/courses/enrollment_demographics_age.html
#: courses/templates/courses/enrollment_demographics_education.html
#: courses/templates/courses/enrollment_demographics_gender.html
#: courses/templates/courses/enrollment_geography.html
msgid "Download CSV"
msgstr "Pobierz CSV"

#: courses/templates/courses/enrollment_activity.html
msgid "Daily Student Enrollment"
msgstr "Dzienny poziom zapisów"

#: courses/templates/courses/enrollment_activity.html
msgid "How many students are in my course?"
msgstr "Ilu studentów jest na moim kursie?"

#: courses/templates/courses/enrollment_activity.html
msgid "Enrollments"
msgstr "Zapisy"

#: courses/templates/courses/enrollment_activity.html
msgid ""
"This graph displays total enrollment for the course calculated at the end of"
" each day. Total enrollment includes new enrollments as well as "
"unenrollments."
msgstr ""
"Ten wykres przedstawia łączną liczbę uczestników kursu, wyliczoną na koniec "
"każdego dnia. Całkowita ilość uczestników obejmuje nowe zarejestrowania oraz"
" wypisania."

#: courses/templates/courses/enrollment_activity.html
msgid "Enrollment Metrics"
msgstr "Statystyki zapisów"

#. Translators: This is a label to identify current student enrollment.
#: courses/templates/courses/enrollment_activity.html
msgid "Total Enrollment"
msgstr "Łącznie uczestników"

#. Translators: This is a label indicating the number of students enrolled in
#. a course.
#: courses/templates/courses/enrollment_activity.html
msgid "Students enrolled in the course."
msgstr "Studentów zapisanych na kurs."

#. Translators: This is a label indicating the change in the number of
#. students enrolled in a course since the previous week.
#: courses/templates/courses/enrollment_activity.html
msgid "Change in Last Week"
msgstr "Zmiana w poprzednim tygodniu"

#: courses/templates/courses/enrollment_activity.html
msgid ""
"The difference between the number of students enrolled at the end of the day"
" yesterday and one week before."
msgstr ""
"Różnica między liczbą zapisanych studentów do wczoraj, a statystyką sprzed "
"tygodnia."

#. Translators: This is a label to identify enrollment of students on the
#. verified track.
#: courses/templates/courses/enrollment_activity.html
msgid "Verified Enrollment"
msgstr "Ścieżka zweryfikowana"

#. Translators: This is a label indicating the number of students enrolled in
#. a course on the verified track.
#: courses/templates/courses/enrollment_activity.html
msgid ""
"Number of enrolled students who are pursuing a verified certificate of "
"achievement."
msgstr ""
"Liczba zapisanych studentów, którzy uczą się w celu uzyskania certyfikatu."

#. Translators: This is a label indicating the change in the number of
#. students enrolled in the verified track of a course since the previous
#. week.
#: courses/templates/courses/enrollment_activity.html
msgid "Change in Verified Enrollments Last Week"
msgstr "Zmiany w ścieżce zweryfikowanej w ostatnim tygodniu"

#: courses/templates/courses/enrollment_activity.html
msgid ""
"The difference between the number of students pursuing verified certificates"
" at the end of the day yesterday and one week before."
msgstr ""
"Różnica między liczbą studentów uczących się w celu uzyskania certyfikatu "
"między dniem wczorajszym a zeszłym tygodniem."

#: courses/templates/courses/enrollment_activity.html
#: courses/templates/courses/enrollment_activity.html
#: courses/templates/courses/enrollment_demographics_age.html
#: courses/templates/courses/enrollment_demographics_education.html
#: courses/templates/courses/enrollment_demographics_gender.html
msgid "Enrollment Over Time"
msgstr "Zapisy w przedziale czasu"

#: courses/templates/courses/enrollment_demographics_age.html
msgid "How old are my students?"
msgstr "Ile lat mają moi studenci?"

#. Translators: Maintain the double percentage symbols (%%) but move them to
#. the appropriate location (before/after the value placeholder) for your
#. language.
#: courses/templates/courses/enrollment_demographics_age.html
msgid ""
"This age histogram presents data computed for the %(value)s%% of enrolled "
"students who provided a year of birth."
msgstr ""
"Ten histogram wieku przedstawia dane dla %(value)s%% spośród studentów, "
"którzy podali w profilu rok urodzenia."

#: courses/templates/courses/enrollment_demographics_age.html
msgid "Age Metrics"
msgstr "Statystyki wieku"

#. Translators: This is a label to identify the median age of enrolled
#. students.
#: courses/templates/courses/enrollment_demographics_age.html
msgid "Median Student Age"
msgstr "Mediana wieku studentów"

#: courses/templates/courses/enrollment_demographics_age.html
msgid ""
"The midpoint of the student ages, computed from the provided year of birth."
msgstr ""
"Średnia wieku studentów na podstawie wprowadzonych do profili dat urodzenia."

#. Translators: This is a label to identify the number of students in the age
#. range.
#: courses/templates/courses/enrollment_demographics_age.html
msgid "Students 25 and Under"
msgstr "Studenci w wieku poniżej 25 lat"

#: courses/templates/courses/enrollment_demographics_age.html
msgid ""
"The percentage of students aged 25 years or younger (of those who provided a"
" year of birth)."
msgstr ""
"Procentowa ilość studentów mających 25 lat lub mniej (spośród wszystkich "
"studentów, którzy podali rok urodzenia)."

#. Translators: This is a label to identify the number of students in the age
#. range.
#: courses/templates/courses/enrollment_demographics_age.html
msgid "Students 26 to 40"
msgstr "Studenci w wieku 26 - 40 lat."

#: courses/templates/courses/enrollment_demographics_age.html
msgid ""
"The percentage of students aged from 26 to 40 years (of those who provided a"
" year of birth)."
msgstr ""
"Procentowa ilość studentów mających od 26 do 40 lat (spośród wszystkich "
"studentów, którzy podali rok urodzenia)."

#. Translators: This is a label to identify the number of students in the age
#. range.
#: courses/templates/courses/enrollment_demographics_age.html
msgid "Students 41 and Over"
msgstr "Studenci w wieku 41 lat lub więcej"

#: courses/templates/courses/enrollment_demographics_age.html
msgid ""
"The percentage of students aged 41 years or older (of those who provided a "
"year of birth)."
msgstr ""
"Procentowa ilość studentów mających 41 lat lub powyżej (spośród wszystkich "
"studentów, którzy podali rok urodzenia)."

#: courses/templates/courses/enrollment_demographics_age.html
msgid "Age Breakdown"
msgstr "Rozkład wieku"

#: courses/templates/courses/enrollment_demographics_education.html
msgid "What level of education do my students have?"
msgstr "Jaki poziom edukacji mają moi studenci?"

#. Translators: Maintain the double percentage symbols (%%) but move them to
#. the appropriate location (before/after the value placeholder) for your
#. language.
#: courses/templates/courses/enrollment_demographics_education.html
msgid ""
"This graph presents data for the %(value)s%% of enrolled students who "
"provided a highest level of education completed."
msgstr ""
"Ten wykres przedstawia dane dla %(value)s%% spośród studentów, którzy podali"
" w profilu poziom swojej edukacji."

#: courses/templates/courses/enrollment_demographics_education.html
msgid "Education Metrics"
msgstr "Statystyki edukacji"

#. Translators: This is a label to identify student educational background.
#: courses/templates/courses/enrollment_demographics_education.html
msgid "High School Diploma or Less"
msgstr "Matura lub niższy"

#: courses/templates/courses/enrollment_demographics_education.html
msgid ""
"The percentage of students who selected Secondary/high school, Junior "
"secondary/junior high/middle school, or Elementary/primary school as their "
"highest level of education completed."
msgstr ""
"Procentowa liczba studentów, którzy wskazali maturę, ukończenie szkoły "
"średniej, ukończenie gimnazjum lub ukończenie szkoły podstawowej jako "
"najwyższy poziom  posiadanej edukacji."

#. Translators: This is a label to identify student educational background.
#: courses/templates/courses/enrollment_demographics_education.html
msgid "College Degree"
msgstr "Podstawowe wykształcenie wyższe"

#: courses/templates/courses/enrollment_demographics_education.html
msgid ""
"The percentage of students who selected Bachelor's degree or Associate "
"degree as their highest level of education completed."
msgstr ""
"Procentowa liczba studentów, którzy wskazali licencjat lub szkołę policealną"
" jako najwyższy poziom posiadanej edukacji."

#. Translators: This is a label to identify student educational background.
#: courses/templates/courses/enrollment_demographics_education.html
msgid "Advanced Degree"
msgstr "Zaawansowane wykształcenie wyższe"

#: courses/templates/courses/enrollment_demographics_education.html
msgid ""
"The percentage of students who selected Doctorate or Master's or "
"professional degree as their highest level of education completed."
msgstr ""
"Procentowa liczba studentów, którzy wskazali magisterium lub doktorat jako "
"najwyższy poziom posiadanej edukacji."

#: courses/templates/courses/enrollment_demographics_education.html
msgid "Educational Breakdown"
msgstr "Rozkład edukacji"

#: courses/templates/courses/enrollment_demographics_gender.html
msgid "What is the student gender breakdown?"
msgstr "Jaka jest płeć moich studentów?"

#. Translators: Maintain the double percentage symbols (%%) but move them to
#. the appropriate location (before/after the value placeholder) for your
#. language.
#: courses/templates/courses/enrollment_demographics_gender.html
msgid ""
"This graph presents data for the %(value)s%% of enrolled students who "
"specified their gender."
msgstr ""
"Ten wykres przedstawia dane dla %(value)s%% spośród studentów, którzy podali"
" w profilu swoją płeć."

#: courses/templates/courses/enrollment_demographics_gender.html
msgid "Gender Breakdown Over Time"
msgstr "Rozkład płci"

#: courses/templates/courses/enrollment_geography.html
msgid "Geographic Distribution"
msgstr "Rozkład geograficzny"

#: courses/templates/courses/enrollment_geography.html
msgid "Where are my students?"
msgstr "Gdzie mieszkają moi studenci?"

#: courses/templates/courses/enrollment_geography.html
msgid "Enrollment by Country"
msgstr "Rekrutacja według kraju"

#: courses/templates/courses/enrollment_geography.html
#: courses/tests/test_views/__init__.py courses/views.py
msgid "Enrollment"
msgstr "Rekrutacja"

#: courses/templates/courses/enrollment_geography.html
msgid "Geography Metrics"
msgstr "Dane geograficzne"

#. Translators: This string represents the number of countries where students
#. are enrolled.
#: courses/templates/courses/enrollment_geography.html
msgid "Total Countries Represented"
msgstr "Suma krajów"

#: courses/templates/courses/enrollment_geography.html
msgid "Countries with at least one student."
msgstr "Kraje z przynajmniej jednym studentem."

#. Translators: This string represents the country with the greatest number of
#. enrolled students.
#: courses/templates/courses/enrollment_geography.html
msgid "Top Country by Enrollment"
msgstr "Czołowy krajów wg liczby studentów"

#: courses/templates/courses/enrollment_geography.html
msgid "Country with the largest number of students."
msgstr "Kraj o największej liczbie studentów"

#. Translators: This string represents the country with the second-greatest
#. number of enrolled students.
#: courses/templates/courses/enrollment_geography.html
msgid "Second Country by Enrollment"
msgstr "Drugi kraj o największej liczbie studentów"

#: courses/templates/courses/enrollment_geography.html
msgid "Country with the second largest number of students."
msgstr "Kraj z drugą największą liczbą studentów."

#. Translators: This string represents the country with the third-greatest
#. number of enrolled students.
#: courses/templates/courses/enrollment_geography.html
msgid "Third Country by Enrollment"
msgstr "Trzeci kraj o największej liczbie studentów"

#: courses/templates/courses/enrollment_geography.html
msgid "Country with the third largest number of students."
msgstr "Kraj z trzecią największą liczbą studentów."

#. Translators: This string represents the percentage of students in a course.
#. Move the % symbol, if necessary, but keep the percent variable/placeholder.
#: courses/templates/courses/enrollment_geography.html
msgid "%(percent)s%% of students"
msgstr "%(percent)s%% studentów"

#: courses/templates/courses/enrollment_geography.html
#: courses/templates/courses/enrollment_geography.html
msgid "Geographic Breakdown"
msgstr "Rozkład geograficzny"

#: courses/templates/courses/index.html
msgid "Courses"
msgstr "Kursy"

#: courses/templates/courses/index.html
msgid "Welcome, %(username)s!"
msgstr "Witaj, %(username)s!"

#: courses/templates/courses/index.html
msgid "Here are the courses you currently have access to in Insights:"
msgstr "Oto lista kursów, do których posiadasz dostęp"

#: courses/templates/courses/index.html
msgid "New to %(application_name)s?"
msgstr "Nowy w %(application_name)s?"

#: courses/templates/courses/index.html
msgid ""
"Click Help in the upper-right corner to get more information about "
"%(application_name)s. Send us feedback at %(email_link)s."
msgstr ""

#. Translators: This message is displayed when the website is unable to get
#. the credentials for the user
#: courses/templates/courses/permissions-retrieval-failed.html
#: courses/templates/courses/permissions-retrieval-failed.html
msgid "Permissions Retrieval Failed"
msgstr ""

#: courses/templates/courses/permissions-retrieval-failed.html
msgid ""
"There was a problem retrieving course permissions for your account. Please "
"try again. If you continue to experience issues, please let us know."
msgstr ""

#: courses/templates/courses/permissions-retrieval-failed.html
#: templates/auth_error.html
msgid "Try Again"
msgstr "Spróbuj ponownie"

#: courses/tests/test_serializers.py
msgid "primary"
msgstr ""

#: courses/tests/test_views/__init__.py courses/views.py
msgid "Activity"
msgstr "Aktywność"

#: courses/tests/test_views/__init__.py courses/views.py
msgid "Demographics"
msgstr "Demografia"

#: courses/tests/test_views/__init__.py courses/views.py
msgid "Geography"
msgstr "Geografia"

#: courses/tests/test_views/__init__.py courses/views.py
msgid "Age"
msgstr "Wiek"

#: courses/tests/test_views/__init__.py courses/views.py
msgid "Education"
msgstr "Edukacja"

#: courses/tests/test_views/__init__.py courses/views.py
msgid "Gender"
msgstr "Płeć"

#: courses/tests/test_views/test_pages.py courses/views.py
msgid "Engagement"
msgstr "Zaangażowanie"

#. Translators: Content as in course content (e.g. things, not the feeling)
#: courses/tests/test_views/test_pages.py courses/views.py
msgid "Content"
msgstr "Zawartość"

#. Translators: Do not translate UTC.
#: courses/views.py
msgid ""
"Demographic student data was last updated %(update_date)s at %(update_time)s"
" UTC."
msgstr ""
"Dane demograficzne studentów zostały uaktualnione %(update_date)s o "
"%(update_time)s UTC."

#. Translators: This sentence is displayed at the bottom of the page and
#. describe the demographics data displayed.
#: courses/views.py
msgid ""
"All above demographic data was self-reported at the time of registration."
msgstr ""

#: courses/views.py
msgid "Enrollment Activity"
msgstr "Aktywność rekrutacyjna"

#. Translators: Do not translate UTC.
#: courses/views.py
msgid ""
"Enrollment activity data was last updated %(update_date)s at %(update_time)s"
" UTC."
msgstr ""
"Dane dotyczące aktywności zapisów zostały zaktualizowane %(update_date)s o "
"%(update_time)s UTC."

#: courses/views.py
msgid "Enrollment Demographics by Age"
msgstr ""

#: courses/views.py
msgid "Enrollment Demographics by Education"
msgstr ""

#: courses/views.py
msgid "Enrollment Demographics by Gender"
msgstr ""

#: courses/views.py
msgid "Enrollment Geography"
msgstr "Geografia rekrutacji"

#. Translators: Do not translate UTC.
#: courses/views.py
msgid ""
"Geographic student data was last updated %(update_date)s at %(update_time)s "
"UTC."
msgstr ""
"Dane dotyczące położenia geograficznego studentów zostały zaktualizowane "
"%(update_date)s o %(update_time)s UTC."

#: courses/views.py
msgid "Engagement Content"
msgstr ""

#. Translators: Do not translate UTC.
#: courses/views.py
msgid ""
"Course engagement data was last updated %(update_date)s at %(update_time)s "
"UTC."
msgstr ""

#: templates/403.html templates/403.html.py
msgid "Access Denied"
msgstr "Odmowa dostępu"

#: templates/403.html
msgid ""
"You do not have permission to view this page. If you believe you should have"
" access to this page, try logging out and logging in again to refresh your "
"permissions."
msgstr ""
"Nie posiadasz uprawnień, by wyświetlić tą stronę. Jeśli uważasz, że "
"powinieneś mieć dostęp do tej strony, spróbuj wylogować się i zalogować "
"ponownie by odświeżyć twoje uprawnienia."

#: templates/403.html
msgid "Logout then Login"
msgstr "Wyloguj się i zaloguj ponownie"

#: templates/404.html templates/404.html.py
msgid "Page Not Found"
msgstr "Strony nie znaleziono"

#: templates/404.html
msgid ""
"We can't find the page you're looking for. Please make sure the URL is "
"correct and if not try again."
msgstr ""
"Nie możemy znaleźć strony, której szukasz. Upewnij się, że URL jest "
"prawidłowy, a jeśli nie, spróbuj ponownie."

#: templates/500.html templates/500.html.py
msgid "An Error Occurred"
msgstr "Wystapił błąd"

#: templates/500.html
msgid ""
"Our servers weren't able to complete your request. Please try again by "
"refreshing this page or retracing your steps."
msgstr ""
"Nasz serwery nie były w stanie wypełnić twojego zlecenia. Spróbuj ponownie, "
"odświeżając tą stronę lub powtarzając poprzednie kroki."

#: templates/auth_error.html templates/auth_error.html.py
msgid "Authentication Failed"
msgstr "Autoryzacja nie powiodła się"

#: templates/auth_error.html
msgid "There was a problem accessing your account. Please try again."
msgstr "Wystąpił problem z dostępem do twojego konta. Spróbuj ponownie."

#: templates/base-error.html
msgid "Use our support tool to ask questions or share feedback."
msgstr ""
"Użyj naszego narzędzia pomocy, aby zadać pytanie lub podzielić się opinią."

#. Translators: This intention here is that a user clicks a button to contact
#. a support group (e.g. edX Support, or MITx Support). Reorder the
#. placeholder as you see fit.
#: templates/base-error.html
msgid "Contact %(platform_name)s Support"
msgstr ""

#. Translators: Application here refers to the web site/application being used
#. (e.g. the dashboard).
#: templates/base.html templates/footer.html templates/lens-navigation.html
msgid "Application"
msgstr "Aplikacja"

#: templates/footer.html
msgid ""
"We want this dashboard to work for you! Need help? "
"%(support_link_start)sVisit our support forum%(support_link_end)s. Comments "
"or suggestions? Email %(email_link)s."
msgstr ""

#: templates/footer.html
msgid "Terms of Service"
msgstr "Warunki korzystania z Serwisu"

#: templates/footer.html
msgid "Privacy Policy"
msgstr "Polityka prywatności"

#. Translators: The © symbol appears directly before this line.
#: templates/footer.html
msgid ""
" %(current_year)s %(platform_name)s, except where noted, all rights\n"
"            reserved. "
msgstr ""

#: templates/header.html
msgid "Toggle navigation"
msgstr "Włącz nawigację"

#: templates/header.html
msgid "Help"
msgstr "Pomoc"

#: templates/header.html
msgid "Logout"
msgstr "Wyloguj się"

#. Translators: This refers to the active tab/navigation item.
#: templates/lens-navigation.html templates/submenu_navigation.html
msgid "Active"
msgstr "Aktywne"

#: templates/loading.html
msgid "LOADING..."
msgstr "WCZYTYWANIE..."

#: templates/registration/logged_out.html
#: templates/registration/logged_out.html
msgid "Logged Out"
msgstr "Wylogowano"

#: templates/registration/logged_out.html
msgid "You have been logged out. Click the button below to login again."
msgstr ""
"Zostałeś wylogowany. Kliknij poniższy przycisk, by zalogować się ponownie."

#: templates/registration/logged_out.html
msgid "Login"
msgstr "Zaloguj się"

#: templates/section_error.html
msgid "We are unable to load this chart."
msgstr "Nie mogliśmy wczytać tego wykresu."

#: templates/section_error.html
msgid "We are unable to load this table."
msgstr "Nie mogliśmy wczytać tej tabeli."

#: templates/section_error.html
msgid "We are unable to load these metrics."
msgstr "Nie możemy załadować tych metryk."
